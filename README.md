# TRIAGE #



### What is TRIAGE? ###

**TRIAGE** is a **GIS** app that locates healthcare providers ranked according to proximity & services within an interactive map.